#!/bin/bash
DISCORD_WEBHOOK="https://discordapp.com/api/webhooks/606482313886564362/YVb1fMJ7y7qgtGj7UJmEPeZI3AIMkJJQsoTHh-eITwRho0ikNkY5HMqTjWcZNorIoe0B"
IP_PROVIDER="ifconfig.co"
LOG_FILE="/opt/ip-cronner/getip.log"

API_TOKEN="0o7TXcO_-h1Ud-RiLMFYnkQwz2ok8_8Wj1KN3IrO"
DOMAIN_NAME="daystram.com"
RECORD_TTL=120
PROXY_ENABLED=false

if [ ! -f "$LOG_FILE" ]; then
	echo "x.x.x.x" > $LOG_FILE
fi

CURRENT_IP=$(curl -s $IP_PROVIDER)
LAST_IP=$(cat $LOG_FILE | tail -n1)

if [ "$CURRENT_IP" != "$LAST_IP" ]; then
	ZONE_DETAIL=$(curl -sX GET "https://api.cloudflare.com/client/v4/zones" \
		-H "Authorization: Bearer $API_TOKEN" \
		-H "Content-Type: application/json")
	ZONE_ID=$(echo $ZONE_DETAIL | grep -Eo '([a-z0-9]{16,})' | head -1)
	RECORD_DETAIL=$(curl -sX GET "https://api.cloudflare.com/client/v4/zones/$ZONE_ID/dns_records?type=A&name=$DOMAIN_NAME&match=all" \
		-H "Authorization: Bearer $API_TOKEN" \
		-H "Content-Type: application/json")
	RECORD_ID=$(echo $RECORD_DETAIL | grep -Eo '([a-z0-9]{16,})' | head -1)
	curl -sX PUT "https://api.cloudflare.com/client/v4/zones/$ZONE_ID/dns_records/$RECORD_ID" \
		-H "Authorization: Bearer $API_TOKEN" \
		-H "Content-Type: application/json" \
		--data "{\"type\":\"A\",\"name\":\"$DOMAIN_NAME\",\"content\":\"$CURRENT_IP\",\"ttl\":$RECORD_TTL,\"proxied\":$PROXY_ENABLED}" > /dev/null

	curl --header "Content-Type: application/json" --request POST --data "{\"content\":\"IP updated to $CURRENT_IP. Cloudflare is propagating new DNS record.\"}" $DISCORD_WEBHOOK
	printf "$(date)\n$CURRENT_IP\n" > $LOG_FILE
fi

