# IP Cronner

---

## Usage
Using `crontab -e` insert the following entry
```
* * * * * bash /PATH_TO_IPCRONNER/ip-cronner/getip.sh
```
